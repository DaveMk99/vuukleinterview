﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VuukleInterView.Interfaces
{
    public interface IRepository<T>
    {
        void Set(string key, T val);

        T Get(string key);
    }
}