﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VuukleInterView.ViewModels
{
    public class BadRequestViewModel
    {
        public IEnumerable<string> ErrorMessages { get; set; }
    }
}