﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VuukleInterView.Interfaces;
using VuukleInterView.Repository;

using VuukleInterView.ViewModels;

namespace VuukleInterView.Jobs
{
    public class AddStringJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            var dataMap = context.JobDetail.JobDataMap;
            var memcacheInstanceName = dataMap.GetString("memcacheInstanceName");
            if (!string.IsNullOrEmpty(memcacheInstanceName))
            {
                var stringRepository = new StringRepository(BeIT.MemCached.MemcachedClient.GetInstance(memcacheInstanceName));
                var key = dataMap.GetString("addStringJobKey");
                var value = dataMap.GetString("addStringJobValue");
                if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                {
                    stringRepository.Set(key, value);
                }
                else
                {
                    await Console.Error.WriteLineAsync("Can't add value");
                }
            }
        }
    }
}