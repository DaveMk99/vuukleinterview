﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VuukleInterView.Interfaces;
using VuukleInterView.Repository;
using VuukleInterView.Utility;
using Xunit;

namespace VuukleInterView.Test
{
    public class TestString
    {
        private static readonly IRepository<string> _stringRepository;
        private static readonly string _memcacheInstanceName = "TestCache";
        private static readonly string _memcacheHost = "167.99.72.173:11212";

        static TestString()
        {
            BeIT.MemCached.MemcachedClient.Setup(_memcacheInstanceName, new string[] { _memcacheHost });

            var memcached = BeIT.MemCached.MemcachedClient.GetInstance(_memcacheInstanceName);
            _stringRepository = new StringRepository(memcached);
        }

        [Fact]
        public void CheckGet()
        {
            var key = Guid.NewGuid().ToString();
            _stringRepository.Set(key, "hello");
            var val = _stringRepository.Get(key);
            Assert.Equal("hello", val);
        }

        [Fact]
        public async Task CheckSet()
        {
            var key = Guid.NewGuid().ToString();
            await QuartzUtility.AddWithTimer(new ViewModels.AddStringViewModel
            {
                Key = key,
                Value = "hello"
            }, _memcacheInstanceName);
            await Task.Delay(100);
            var val = _stringRepository.Get(key);
            Assert.Equal("hello", val);
        }
    }
}