﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VuukleInterView.Interfaces;
using VuukleInterView.Repository;
using VuukleInterView.Utility;
using VuukleInterView.ViewModels;

namespace VuukleInterView.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StringController : ControllerBase
    {
        private readonly IRepository<string> _stringRepository;
        private readonly IConfiguration _configuration;

        public StringController(BeIT.MemCached.MemcachedClient memcachedClient, IConfiguration configuration)
        {
            _stringRepository = new StringRepository(memcachedClient);
            _configuration = configuration;
        }

        [HttpGet("{key}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(void), 404)]
        [ProducesResponseType(typeof(BadRequestViewModel), 400)]
        public IActionResult GetByKey(string key)
        {
            if (string.IsNullOrEmpty(key) || key.Contains(" "))
            {
                var badRequestViewModel = new BadRequestViewModel
                {
                    ErrorMessages = new string[] { "Key format is invalid" }
                };
                return BadRequest(badRequestViewModel);
            }

            var result = _stringRepository.Get(key);

            if (string.IsNullOrEmpty(result))
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), 200)]
        [ProducesResponseType(typeof(BadRequestViewModel), 400)]
        public async Task<IActionResult> Add([FromBody]AddStringViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                var badRequestViewModel = new BadRequestViewModel
                {
                    ErrorMessages = ValidationUtility.GetErrorMessages(ModelState)
                };
                return BadRequest(badRequestViewModel);
            }
            await QuartzUtility.AddWithTimer(viewModel, _configuration.GetValue<string>("MemcacheName"));
            return Ok();
        }
    }
}