﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VuukleInterView.Utility
{
    public static class ValidationUtility
    {
        public static IEnumerable<string> GetErrorMessages(ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(x => x.Errors)
                .Select(x => x.ErrorMessage);
        }
    }
}