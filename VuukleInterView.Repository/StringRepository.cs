﻿using System;
using System.Collections.Generic;
using System.Text;
using VuukleInterView.Interfaces;
using BeIT.MemCached;

namespace VuukleInterView.Repository

{
    public class StringRepository : IRepository<string>
    {
        private readonly MemcachedClient _memcachedClient;

        public StringRepository(MemcachedClient memcachedClient)
        {
            _memcachedClient = memcachedClient;
        }

        public string Get(string key)
        {
            var stats = _memcachedClient.Stats();
            var val = _memcachedClient.Get(key);
            return val as string;
        }

        public void Set(string key, string val)
        {
            _memcachedClient.Set(key, val);
        }
    }
}