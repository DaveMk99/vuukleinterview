﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VuukleInterView.ViewModels
{
    public class AddStringViewModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}