﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VuukleInterView.Interfaces;
using VuukleInterView.Jobs;
using VuukleInterView.Repository;
using VuukleInterView.ViewModels;

namespace VuukleInterView.Utility
{
    public static class QuartzUtility
    {
        private static IScheduler _scheduler;

        static QuartzUtility()
        {
            var props = new NameValueCollection
                        {
                            { "quartz.serializer.type", "binary" }
                        };
            var factory = new StdSchedulerFactory(props);
            _scheduler = factory.GetScheduler().Result;
            _scheduler.Start().Wait();
        }

        public static async Task AddWithTimer(AddStringViewModel viewModel, string memcacheInstanceName)
        {
            var jobKey = "addStringJob" + Guid.NewGuid().ToString();
            var job = JobBuilder.Create<AddStringJob>()
                  .WithIdentity(jobKey, "stringGroup")
                  .UsingJobData("addStringJobKey", viewModel.Key)
                  .UsingJobData("addStringJobValue", viewModel.Value)
                  .UsingJobData("memcacheInstanceName", memcacheInstanceName)
                  .Build();
            ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("addString", "stringGroup")
                    .StartNow()
                    .Build();

            await _scheduler.ScheduleJob(job, trigger);
        }
    }
}